# EPAM_102_1_Blueprint

Developed with Unreal Engine 5

Blueprints requirements: 

Opening Door blueprint: 

- Door frame Static Mesh 
- Door Static mesh 
- Logic inside the Event Graph that will play the animation of the door opening and closing bind to a keyboard key 
- Blueprint utilizes the Collision Box (Event Begin Overlap) 
- Timeline curve 
- Logic that will determine if door is locked/unlocked 

Door Switch blueprint: 

- Switch box Static Mesh 
- Blueprint utilizes the Collision Box (Event Begin Overlap) 
- Logic that will lock/unlock the Door BP 

Success criteria: 

- The Door (when unlocked) opens and closes when specified keyboard key is pressed 
- The Door (when locked) won’t open/close when specified keyboard key is pressed 
- The Switch box BP locks/unlocks the Door when specified keyboard key is pressed 
- Door plays opening and closing animation 
- When Door opens a message is printed on the screen 
- When Door is locked a message is printed on the screen 

Recreate the aforementioned functionality using the C++ code

![Level.](/readme/lvl.PNG "Level.")

![Lock_BP.](/readme/Lock_BP.PNG "Lock Blueprint.")

![Door_BP.](/readme/Door_BP.PNG "Door Blueprint.")

Demo: https://youtu.be/YqAls_MpS_Q