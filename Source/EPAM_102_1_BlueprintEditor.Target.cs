// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class EPAM_102_1_BlueprintEditorTarget : TargetRules
{
	public EPAM_102_1_BlueprintEditorTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "EPAM_102_1_Blueprint" } );
	}
}
