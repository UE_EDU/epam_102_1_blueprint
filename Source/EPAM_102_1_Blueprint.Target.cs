// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class EPAM_102_1_BlueprintTarget : TargetRules
{
	public EPAM_102_1_BlueprintTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.AddRange( new string[] { "EPAM_102_1_Blueprint" } );
	}
}
