// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Components/TimelineComponent.h"
#include "Door_CPP.generated.h"

UCLASS()
class EPAM_102_1_BLUEPRINT_API ADoor_CPP : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ADoor_CPP();

	bool isUnlocked = false;
	bool isClosing = false;

	UFUNCTION()
		void TimelineProgress(float Value);

	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
		void OpenClose();

private:

	FTimeline CurveFTimeline;

	UPROPERTY()
		UCurveFloat* CurveFloat;

	UPROPERTY()
		UStaticMeshComponent* SM_DoorFrame;

	UPROPERTY()
		UStaticMeshComponent* SM_Door;

	UPROPERTY()
		UBoxComponent* Box;

	UFUNCTION()
		void OnOverlapBegin(
			UPrimitiveComponent* OverlappedComp,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult
		);

	UFUNCTION()
		void OnOverlapEnd(
			UPrimitiveComponent* OverlappedComp,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex
		);
};
