// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EPAM_102_1_BlueprintGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class EPAM_102_1_BLUEPRINT_API AEPAM_102_1_BlueprintGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
