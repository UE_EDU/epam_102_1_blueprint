// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Door_CPP.h"
#include "Lock_CPP.generated.h"

UCLASS()
class EPAM_102_1_BLUEPRINT_API ALock_CPP : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALock_CPP();

	UPROPERTY(EditAnywhere)
		ADoor_CPP* DoorTarget;

private:

	UPROPERTY()
		UStaticMeshComponent* SM_Btn;

	UPROPERTY()
		UBoxComponent* Box;

	UFUNCTION()
		void LockUnlock();

	UFUNCTION()
		void OnOverlapBegin(
			UPrimitiveComponent* OverlappedComp,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex,
			bool bFromSweep,
			const FHitResult& SweepResult
		);

	UFUNCTION()
		void OnOverlapEnd(
			UPrimitiveComponent* OverlappedComp,
			AActor* OtherActor,
			UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex
		);
};
