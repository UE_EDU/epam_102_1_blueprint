// Fill out your copyright notice in the Description page of Project Settings.


#include "Lock_CPP.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ALock_CPP::ALock_CPP()
{
	SM_Btn = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SM_Btn"));
	RootComponent = SM_Btn;
	ConstructorHelpers::FObjectFinder<UStaticMesh> SM_Mat_Preview(TEXT("/Game/StarterContent/Props/SM_MatPreviewMesh_02"));
	SM_Btn->SetStaticMesh(SM_Mat_Preview.Object);

	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	Box->SetupAttachment(SM_Btn);
	Box->SetRelativeLocation({ 110, 0, 190 });
	Box->SetRelativeScale3D({ 5, 3.5, 5 });
	Box->OnComponentBeginOverlap.AddDynamic(this, &ALock_CPP::OnOverlapBegin);
	Box->OnComponentEndOverlap.AddDynamic(this, &ALock_CPP::OnOverlapEnd);

	DoorTarget = CreateDefaultSubobject<ADoor_CPP>(TEXT("DoorTarget"));

	if (!InputComponent)
	{
		InputComponent = CreateDefaultSubobject<UInputComponent>(TEXT("DoorInput"));
		InputComponent->RegisterComponent();
	}

	InputComponent->BindKey(EKeys::E, IE_Pressed, this, &ALock_CPP::LockUnlock);
}

void ALock_CPP::LockUnlock()
{
	DoorTarget->isUnlocked = !(DoorTarget->isUnlocked);

	if (DoorTarget->isUnlocked)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Green, TEXT("Lock deactivated"));
		SM_Btn->SetVectorParameterValueOnMaterials(TEXT("Color"), { 0, 100, 0 });
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Lock activated"));
		SM_Btn->SetVectorParameterValueOnMaterials(TEXT("Color"), { 100, 0, 0 });
	}
}

void ALock_CPP::OnOverlapBegin(
	UPrimitiveComponent* OverlappedComp,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult
)
{
	EnableInput(UGameplayStatics::GetPlayerController(this, 0));
}

void ALock_CPP::OnOverlapEnd(
	UPrimitiveComponent* OverlappedComp,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex
)
{
	DisableInput(UGameplayStatics::GetPlayerController(this, 0));
}