// Fill out your copyright notice in the Description page of Project Settings.


#include "Door_CPP.h"
#include "Kismet/GameplayStatics.h"
#include "Components/TimelineComponent.h"

// Sets default values
ADoor_CPP::ADoor_CPP()
{
	PrimaryActorTick.bCanEverTick = true;

	SM_DoorFrame = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SM_DoorFrame"));
	RootComponent = SM_DoorFrame;
	ConstructorHelpers::FObjectFinder<UStaticMesh> SM_DoorFrameProp(TEXT("/Game/StarterContent/Props/SM_DoorFrame"));
	SM_DoorFrame->SetStaticMesh(SM_DoorFrameProp.Object);

	SM_Door = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SM_Door"));
	SM_Door->SetupAttachment(SM_DoorFrame);
	ConstructorHelpers::FObjectFinder<UStaticMesh> SM_DoorProp(TEXT("/Game/StarterContent/Props/SM_Door"));
	SM_Door->SetStaticMesh(SM_DoorProp.Object);
	SM_Door->SetRelativeLocation({ 0,45,0 });

	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("Box"));
	Box->SetupAttachment(SM_DoorFrame);
	Box->SetRelativeLocation({ 0, 0, 100 });
	Box->SetRelativeScale3D({ 4, 1.25, 2.75 });
	Box->OnComponentBeginOverlap.AddDynamic(this, &ADoor_CPP::OnOverlapBegin);
	Box->OnComponentEndOverlap.AddDynamic(this, &ADoor_CPP::OnOverlapEnd);

	if (!InputComponent)
	{
		InputComponent = CreateDefaultSubobject<UInputComponent>(TEXT("DoorInput"));
		InputComponent->RegisterComponent();
	}

	InputComponent->BindKey(EKeys::E, IE_Pressed, this, &ADoor_CPP::OpenClose);

	ConstructorHelpers::FObjectFinder<UCurveFloat> DoorRotCurve(TEXT("/Game/Misc/DoorRotCurve"));
	CurveFloat = DoorRotCurve.Object;
	FOnTimelineFloat TimelineProgress;
	TimelineProgress.BindUFunction(this, FName("TimelineProgress"));
	CurveFTimeline.AddInterpFloat(CurveFloat, TimelineProgress);
}

void ADoor_CPP::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	CurveFTimeline.TickTimeline(DeltaTime);
}

void ADoor_CPP::TimelineProgress(float Value)
{
	SM_Door->SetRelativeRotation({ 0,90 * Value,0 });
}

void ADoor_CPP::OpenClose()
{
	if (!isUnlocked)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, TEXT("Door is locked"));
		return;
	}

	if (isClosing)
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Door closing"));
		CurveFTimeline.Reverse();
	}
	else
	{
		GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Blue, TEXT("Door opening"));
		CurveFTimeline.Play();
	}

	isClosing = !isClosing;
}

void ADoor_CPP::OnOverlapBegin(
	UPrimitiveComponent* OverlappedComp,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex,
	bool bFromSweep,
	const FHitResult& SweepResult
)
{
	EnableInput(UGameplayStatics::GetPlayerController(this, 0));
}

void ADoor_CPP::OnOverlapEnd(
	UPrimitiveComponent* OverlappedComp,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex
)
{
	DisableInput(UGameplayStatics::GetPlayerController(this, 0));
}